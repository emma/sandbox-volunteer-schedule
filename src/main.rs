#![feature(extern_prelude)]

extern crate csv;
#[macro_use]
extern crate serde_derive;
extern crate argparse;
extern crate handlebars;
extern crate iron;
extern crate mime;
extern crate mount;
extern crate serde;
extern crate serde_json;
extern crate staticfile;
extern crate hsluv;

mod render_template;
mod serve;

use argparse::{ArgumentParser};

#[derive(Clone)]
pub struct Config {
  data_filename: String,
  template_filename: String,
  server_address: String,
  static_file_directory: String,
  mount_point: String,
}

fn main() {
  let mut config = Config {
    data_filename: String::from("data/volunteers.csv"),
    template_filename: String::from("templates/index.hbs"),
    server_address: String::from("localhost:3000"),
    static_file_directory: String::from("static"),
    mount_point: String::from("/"),
  };

  {
    let mut ap = ArgumentParser::new();
    ap.set_description("Generate and serve the Technology Sandbox volunteer schedule.");
    ap.refer(&mut config.data_filename).add_option(
      &["-d", "--data"],
      argparse::Store,
      "the CSV file containing volunteer information",
    );
    ap.refer(&mut config.template_filename).add_option(
      &["-t", "--template"],
      argparse::Store,
      "the handlebars template",
    );
    ap.refer(&mut config.server_address).add_option(
      &["-a", "--address"],
      argparse::Store,
      "the address on which the site is served",
    );
    ap.refer(&mut config.mount_point).add_option(
      &["-m", "--mount-point"],
      argparse::Store,
      "the base path that the site is being served on",
    );
    ap.parse_args_or_exit();
  }

  serve::init(config);
}
