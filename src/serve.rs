use iron::prelude::*;
use iron::status;

use mount::Mount;
use staticfile::Static;
use std::path::Path;

use render_template;
use Config;

fn make_handler(config: &Config) -> impl Fn(&mut Request) -> IronResult<Response> {
  let static_file_handler = Static::new(Path::new(&config.static_file_directory));
  let config = config.clone();

  move |req: &mut Request| {
    use iron::mime;
    use iron::Handler;
    println!("{:?}", &req.url.path());
    match req.url.path()[0] {
      "" | "index.html" => Ok(Response::with((
        mime::Mime(
          iron::mime::TopLevel::Text,
          iron::mime::SubLevel::Html,
          vec![],
        ),
        status::Ok,
        render_template::render_template(&config.clone()),
      ))),
      _ => static_file_handler.handle(req),
    }
  }
}

pub fn init(config: Config) {
  let mut mount = Mount::new();
  mount.mount(&config.mount_point, make_handler(&config));

  println!(
    "Server running at http://{}{}",
    &config.server_address, &config.mount_point
  );

  Iron::new(mount).http(&config.server_address).unwrap();
}
